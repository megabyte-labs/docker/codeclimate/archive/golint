.PHONY: update image

IMAGE_NAME ?= codeclimate/codeclimate-golint

SLIM_IMAGE_NAME ?= codeclimate/codeclimate-golint:slim


image:
	docker build --rm -t $(IMAGE_NAME) .
slim: image
	docker-slim build --tag $(SLIM_IMAGE_NAME) --http-probe=false --exec '/usr/src/app/codeclimate-golint' --mount "$$PWD/tests:/code" --workdir '/code' --include-path '/usr/local/go/bin/go' $(IMAGE_NAME) && prettier --write slim.report.json

test: slim
	container-structure-test test --image $(IMAGE_NAME) --config tests/container-test-config.yaml && container-structure-test test --image $(SLIM_IMAGE_NAME) --config tests/container-test-config.yaml


